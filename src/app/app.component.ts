import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Pages } from './interfaces/pages';
import { AdMobFree, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free/ngx';
import { environment } from 'src/environments/environment.prod';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { File } from '@ionic-native/file/ngx';
import { MainServicesService } from './pages/main-services.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public appPages: Array<Pages>;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    private admobFree: AdMobFree,
    private androidPermissions: AndroidPermissions,
    private file: File,
    private mainServicesService: MainServicesService
  ) {
    // {
    //   title: 'View In Map',
    //   url: '/about',
    //   direct: 'forward',
    //   icon: 'information-circle-outline'
    // },
    this.appPages = [
      {
        title: 'Home',
        url: '/home-results',
        direct: 'root',
        icon: 'home'
      },
      {
        title: 'Tracked List',
        url: '/settings',
        direct: 'forward',
        icon: 'list-box'
      },
      {
        title: 'Help',
        url: '/help',
        direct: 'forward',
        icon: 'help-circle'
      }
    ];

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
        result => {
          if (result && result.hasPermission) {
            console.log('Has permission?', result.hasPermission);
            this.createDir();
          } else {
            this.androidPermissions.requestPermissions(
              [
                this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION,
                this.androidPermissions.PERMISSION.GET_ACCOUNTS,
                this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
                this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
              ]
            ).then((res) => {
              this.createDir();
            });
          }
        },
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
      );
    }).catch(() => { });
  }

  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }
  createDir() {
    this.platform.ready().then(() => {
      if (this.platform.is('android')) {
        this.file.checkDir(this.file.externalRootDirectory, environment.downloadDirectory).then(response => {
          console.log('Directory exists' + response);
        }).catch(err => {
          console.log('Directory doesn\'t exist' + JSON.stringify(err));
          this.file.createDir(this.file.externalRootDirectory, environment.downloadDirectory, false).then(response => {
            console.log('Directory create' + response);
          }).catch(err => {
            console.log('Directory no create' + JSON.stringify(err));
          });
        });
      }
    });
  }
  logout() {
    this.navCtrl.navigateRoot('/');
  }
  fullPageAdd() {
    const interstitialConfig: AdMobFreeInterstitialConfig = {
      // add your config here
      // for the sake of this example we will just use the test config
      id: environment.admobInterstitialKey,
      isTesting: environment.isTestAddmob,
      autoShow: true
    };
    if (this.mainServicesService.addClickCount === 0 || (environment.fullPageAddFreequance <= this.mainServicesService.addClickCount)) {
      this.admobFree.interstitial.config(interstitialConfig);
      this.admobFree.interstitial.prepare().then(() => {
        this.admobFree.interstitial.show();
      });
      if (this.mainServicesService.addClickCount === 0) {
        this.mainServicesService.addClickCount = this.mainServicesService.addClickCount + 1;
      } else {
        this.mainServicesService.addClickCount = 0;
      }

    } else {
      this.mainServicesService.addClickCount = this.mainServicesService.addClickCount + 1;
    }
  }
}
