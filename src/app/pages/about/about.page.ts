import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MainServicesService } from '../main-services.service';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController, ToastController, Platform, PopoverController, ModalController, LoadingController } from '@ionic/angular';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsComponent } from 'src/app/components/notifications/notifications.component';
import { SearchFilterPage } from '../modal/search-filter/search-filter.page';
import { File } from '@ionic-native/file/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { AdMobFreeRewardVideoConfig, AdMobFree, AdMobFreeInterstitialConfig, AdMobFreeBannerConfig } from '@ionic-native/admob-free/ngx';
import { environment } from 'src/environments/environment.prod';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { async } from '@angular/core/testing';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
declare var MarkerClusterer: any;
declare const Buffer;
declare var google;
@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  trainData: any = {};
  originalTrainData: any = {};
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  address: string;
  museums: [];
  databaseObj: SQLiteObject;
  latituede = -31.083332;
  longitude = 150.916672;
  activeSaveButton = false;
  changeItemList: any[] = [];
  isInAppDataLoad = false;
  tempData: any = {}
  loading: any;
  dataId: number = 0;
  customDateFile: any;
  markerCluster: any;
  currentMarkerNumber = 0;
  canMarkerActive = false;
  canMarkerRemove = false;
  readonly database_name: string = "tracker.db";
  readonly table_name: string = "tracker_points";
  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    public toastController: ToastController, public alertCtrl: AlertController, public _MainServicesService: MainServicesService, private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private route: ActivatedRoute, private router: Router,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    private file: File,
    private emailComposer: EmailComposer,
    private admobFree: AdMobFree,
    private fileChooser: FileChooser,
    private filePath: FilePath,
    public loadingController: LoadingController,
    private androidPermissions: AndroidPermissions,
    private mainServicesService: MainServicesService) {

    this.platform.ready().then(() => {
      // this.presentLoading();
      let id = this.route.snapshot.paramMap.get('id');

      console.log("id***************************");
      console.log(id);
      console.log("id***************************");
      if (parseInt(id) === -11) {
        // this.getCustomLoadMarkers(this.tempData.features);
        this.isInAppDataLoad = true;
      } else {
        this.isInAppDataLoad = false;
      }
      this.hideBannerAdd();
      this.createDB(id);
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
        result => {
          console.log('Has permission?', result.hasPermission);
          if (result && result.hasPermission) {
            this.createDir();
            console.log('has per');
          } else {
            this.androidPermissions.requestPermissions(
              [
                this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
                this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
              ]
            ).then((res)=>{
              this.createDir();
            });
          }
        },
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
      );
      // this.dowloadFile();
      // this.route.queryParams.subscribe(params => {
      //   if (params && params.pid) {
      //     let id = JSON.parse(params.pid);
      //     console.log(id);
      //     this.createDB(id);
      //     console.log("id***************************");
      //   }
      // });
    }).catch(error => {
      console.log(error);
    });
  }
  createDir(){
    this.platform.ready().then(() =>{
      if(this.platform.is('android')) {
        this.file.checkDir(this.file.externalRootDirectory, environment.downloadDirectory).then(response => {
          console.log('Directory exists'+response);
        }).catch(err => {
          console.log('Directory doesn\'t exist'+JSON.stringify(err));
          this.file.createDir(this.file.externalRootDirectory, environment.downloadDirectory, false).then(response => {
            console.log('Directory create'+response);
          }).catch(err => {
            console.log('Directory no create'+JSON.stringify(err));
          }); 
        });
      }
    });
  }
  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });
    await this.loading.present();
    console.log('Loading dismissed!');
  }
  openFile() {
    this.fileChooser.open()
      .then(uri => {
        this.filePath.resolveNativePath(uri).then(resolvedFilePath => {
          console.log(resolvedFilePath);
          console.log(resolvedFilePath.split("/"));
          console.log(resolvedFilePath.split("/").pop());
          console.log(resolvedFilePath.substring(0, resolvedFilePath.lastIndexOf("/")));

          this.file.readAsText(resolvedFilePath.substring(0, resolvedFilePath.lastIndexOf("/")), resolvedFilePath.split("/").pop()).then(readText => {
            console.log(readText);

            this.tempData = JSON.parse(readText);
            // this.getCustomLoadMarkers(this.tempData.features);
            this.trainData['Points'] = this.convertGeoJsonToLocalType(this.tempData.features);
            this.setLonLat(this.trainData.Points);
            this.map = null;
            this.loadMap();
            this.getClusterMarkers(null);
          });
        });
      })
      .catch(e => console.log(e));
  }

  convertGeoJsonToLocalType(geoJsonArray: any[]) {
    let tempArray = [];
    geoJsonArray.forEach(element => {
      tempArray.push({ "type": "Point", "coordinates": [element.geometry.coordinates[0], element.geometry.coordinates[1]] });
    });
    return tempArray;
  }
  sendEmailCheckType() {
    this.onconfirmEmail(`<ul>
    <li>'GEO Json file' :- As attachment</li>
    <li>'GEO Json String' :- Geo Json will be added to email body</li>
  </ul>  `, 'Share with Gmail');
  }
  async onconfirmEmail(message, subHeader) {
    let alert = await this.alertCtrl.create({
      message: message,
      subHeader: subHeader,
      buttons: [
        
        {

          text: 'GEO Json file',
          handler: () => {
            this.sendEmail(true);
            console.log('Buy clicked');
          }
        },
        {
          text: 'GEO Json String',
          handler: () => {
            this.sendEmail(false);
            console.log('Buy clicked');
          }
        },
        {
          text: "Cancel",
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }
  sendEmail(asFile) {
    let geoJsonTemp: any = this.creteGeoJson();
    let email = {};
    if (asFile) {
      let objJsonB64 = this.dowloadFile(false);
      email = {
        to: '',
        attachments: [objJsonB64.split('~')[0] + objJsonB64.split('~')[1]],
        subject: 'GEO JSON',
        body: 'GEO Json',
        isHtml: true
      }

      this.emailComposer.open(email).then(result => {
        console.log("**************result");
        console.log(result);
        console.log("**************result");
        this.removeFile(objJsonB64.split('~')[1]);
      });
    } else {
      email = {
        to: '',
        attachments: [],
        subject: 'GEO JSON',
        body: JSON.stringify(geoJsonTemp),
        isHtml: true
      }

      this.emailComposer.open(email).then(result => {
        console.log("**************result");
        console.log(result);
        console.log("**************result");
      });
    }

  }
  removeFile(fileName) {
    this.file.removeFile(this.file.externalRootDirectory + environment.downloadDirectory+'/', fileName)
      .then(_ => {
        // this.presentToastWithOptions('Directory is removed-> ' + this.file.applicationStorageDirectory, 'success');
      }
      ).catch(err => {
        // this.presentToastWithOptions('Directory not exist removed-> ' + this.file.applicationStorageDirectory, 'danger');
      }
      );
  }
  async searchFilter() {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage,
      componentProps: {
        list: this.trainData
      }
    });
    modal.onDidDismiss().then(data => {
      console.log(data);
      if (data.data.isAction && this.trainData.Points) {
        console.log(this.trainData.Points.length);
        if (data.data.isRemove) {
          console.log("before isRemove--> " + this.trainData.Points.length);
          this.trainData.Points = this.removeDataAndLoadMap(data.data.result);
          this.activeSaveButton = true;
          this.markerCluster.clearMarkers();
          this.getClusterMarkers(null);
          console.log("after isRemove--> " + this.trainData.Points.length);
        } else {
          console.log("before--> " + this.trainData.Points.length);
          this.getMiddleMarkersForGivenRange(data.data.from, data.data.to)
          console.log("after--> " + this.trainData.Points.length);
          this.getClusterMarkers(null);
        }

        this.activeSaveButton = true;
        this.presentToastWithOptions("If you are ok with this, don't forget to save after you done..!", "warning");
      } else {
        this.activeSaveButton = false;
      }

      if (data.data.isAction && this.isInAppDataLoad) {
        console.log(this.tempData.features.length);
        if (data.data.isRemove) {
          console.log("before isRemove--> " + this.tempData.features.length);
          this.tempData.features = this.removeDataAndLoadMap(data.data.result);
          this.getMarkers(this.tempData.features, 'name cusom edit');
          console.log("after isRemove--> " + this.tempData.features.length);
        } else {
          console.log("before--> " + this.tempData.features.length);
          this.getMiddleMarkersForGivenRange(data.data.from, data.data.to)
          console.log("after--> " + this.tempData.features.length);
          this.getMarkers(this.tempData.features, 'name custom');
        }

        this.activeSaveButton = true;
        this.presentToastWithOptions("If you are ok with this, don't forget to save after you done..!", "warning");
      } else {
        this.activeSaveButton = false;
      }
    });
    return await modal.present();
  }
  dowloadFile(isMesageWant) {
    let geoJsonTemp = this.creteGeoJson();
    let fileName = 'geojson_' + (new Date()).getTime() + '.json';
    this.file.writeFile(this.file.externalRootDirectory + environment.downloadDirectory+'/', fileName, JSON.stringify(geoJsonTemp), { replace: true })
      .then(_ => {
        if (isMesageWant) {
          this.presentToastWithOptions("Successfull... check your '"+environment.downloadDirectory+"' folder ", 'success', true);
        }
      }
      ).catch(err => {
        if (isMesageWant) {
          this.presentToastWithOptions("'"+environment.downloadDirectory+ "' directory doesn't exsist", 'danger');
        }
      }
      );
    return this.file.externalRootDirectory + environment.downloadDirectory+'/' + `~` + fileName;
  }
  creteGeoJson() {
    if (this.isInAppDataLoad) {
      return this.tempData;
    } else {
      let geoJson = {
        "type": "FeatureCollection",
        "features": []
      };


      this.trainData.Points.forEach((element, index) => {
        let temp = {
          "type": "Feature",
          "geometry": { "type": "Point", "coordinates": [element.coordinates[0], element.coordinates[1]] },
          "properties": {
            "name": index + ''
          }
        };
        geoJson.features.push(temp);
      });
      return geoJson;
    }
  }
  midpoint(latitude1, longitude1, latitude2, longitude2) {
    var DEG_TO_RAD = Math.PI / 180;     // To convert degrees to radians.

    // Convert latitude and longitudes to radians:
    var lat1 = latitude1 * DEG_TO_RAD;
    var lat2 = latitude2 * DEG_TO_RAD;
    var lng1 = longitude1 * DEG_TO_RAD;
    var dLng = (longitude2 - longitude1) * DEG_TO_RAD;  // Diff in longtitude.

    // Calculate mid-point:
    var bx = Math.cos(lat2) * Math.cos(dLng);
    var by = Math.cos(lat2) * Math.sin(dLng);
    var lat = Math.atan2(
      Math.sin(lat1) + Math.sin(lat2),
      Math.sqrt((Math.cos(lat1) + bx) * (Math.cos(lat1) + bx) + by * by));
    var lng = lng1 + Math.atan2(by, Math.cos(lat1) + bx);

    return new google.maps.LatLng(lat / DEG_TO_RAD, lng / DEG_TO_RAD);
  };

  editPointsToMiddle(latitudeOne, longitudeOne, latitudeTwo, longitudeTwo,) {
    let y = this.midpoint(latitudeOne, longitudeOne, latitudeTwo, longitudeTwo);
    console.log(y.lat());
    console.log(y.lng());
    console.log("y%%%%%%%%%%%%%%%%%%%%%%%%555");
    return { longitude: y.lng(), latitude: y.lat() }
  }
  getMiddleMarkersForGivenRange(start, end) {
    let count = JSON.parse(JSON.stringify(start));
    while (count < end) {
      // "coordinates": [
      //   resp.coords.longitude,
      //   resp.coords.latitude
      // ]
      if (this.trainData.Points[count + 1]) {
        let lt1 = this.trainData.Points[count].coordinates[1];
        let ln1 = this.trainData.Points[count].coordinates[0];
        let lt2 = this.trainData.Points[count + 1].coordinates[1];
        let ln2 = this.trainData.Points[count + 1].coordinates[0];

        let middlePoint: any = this.editPointsToMiddle(lt1, ln1, lt2, ln2);

        this.trainData.Points.splice(count + 1, 0, { coordinates: [middlePoint.longitude, middlePoint.latitude] });
        console.log(middlePoint);
      }
      count += 1;

    }
  }

  ngOnInit() {
    // this.getAllTrains();
  }
  updateRecord(item) {
    //     UPDATE Customers
    // SET ContactName = 'Alfred Schmidt', City= 'Frankfurt'
    // WHERE CustomerID = 1;
    return new Promise((resolve, reject) => {
      this.databaseObj.executeSql(`
  UPDATE ${this.table_name} SET Name = '${item.Name}' , Points = '${JSON.stringify(item.Points)}' WHERE pid = ${item.pid}
  `
        , [])
        .then((res) => {
          this.presentToastWithOptions("GEO Json successfully updated", "success");
          this.activeSaveButton = true;
          resolve(true);
        })
        .catch(e => {
          this.presentToastWithOptions("Error updating GEO Json", "danger");
          this.activeSaveButton = false;
          resolve(false);
        });
    });
  }
  removeDataAndLoadMap(filterArray: any[]) {
    let newArr = [];
    for (let i = 0; i <= this.trainData.Points.length - 1; i++) {
      if (!(filterArray.includes(i))) {
        newArr.push(this.trainData.Points[i]);
      }
    }

    return newArr;
  }
  async notifications(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: NotificationsComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }
  refresh() {
    this.getRows(this.dataId).then(async res => {
      if (this.markerCluster) {
        this.markerCluster.clearMarkers();
      }
      this.getClusterMarkers(null);
    });
  }
  async getClusterMarkers(points) {
    if (!this.markerCluster) {
      this.markerCluster = new MarkerClusterer(this.map, [], {
        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
      });
    }
    this.getLonLat(this.trainData.Points).map((location, i) => {
      let museumMarker = new google.maps.Marker({
        position: location,
        label: i + '',
        draggable: true
      });
      google.maps.event.addListener(museumMarker, 'dragend', () => {
        console.log(museumMarker.getPosition().lat());
        if (!this.isInAppDataLoad) {
          this.modifyMarker(museumMarker);
          console.log();
        } else {
          this.modifyCustomLoadMarker(museumMarker);
        }
      });
      google.maps.event.addListener(museumMarker, 'click', () => {
        this.presentConfirmTempRemoveMarker('Do you want to remove ' + museumMarker.label + ' marker?', 'Please make shure to save after your changes', 'No', 'Remove', museumMarker.label);
      });
      this.markerCluster.addMarker(museumMarker);
    });


    console.log(this.markerCluster);
  }
  async getClusterMarkersCustom(points) {
    var markers = this.getLonLat(points).map(function (location, i) {
      return new google.maps.Marker({
        position: location,
        label: i + ''
      });
    });
    var markerCluster = new MarkerClusterer(this.map, markers, {
      imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
    });
  }
  // Retrieve rows from table
  createDB(id) {
    this.sqlite.create({
      name: this.database_name,
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.databaseObj = db;
        // this.presentToastWithOptions("DB created", "success");
        // this.createTable();
        this.dataId = parseInt(id);
        if (parseInt(id) !== -11) {
          this.getRows(id).then(async res => {
            await this.loadingController.create({
              cssClass: 'my-custom-class',
              message: 'Please wait...'
            }).then((loading) => {
              loading.present();
              let tepTData = JSON.parse(JSON.stringify(this.trainData));
              let tepTempData = JSON.parse(JSON.stringify(this.tempData));
              this.trainData = null;
              this.tempData = null;

              this.trainData = tepTData;
              this.tempData = tepTempData;
              this.setLonLat(this.trainData.Points);
              this.map = null;
              this.loadMap();
              this.getClusterMarkers(this.trainData.Points);
              loading.dismiss();
            });
            // await this.loading.present();
            // this.getMarkers(this.trainData.Points, this.trainData.Name);
            // this.originalTrainData = JSON.stringify(JSON.parse(this.trainData));

          });
        } else {
          this.loadMap();
        }

      })
      .catch(e => {
        // this.presentToastWithOptions("DB creation error in function", "danger");
      });
  }
  getRows(id) {
    return new Promise((resolve, reject) => {
      let pid = parseInt(id);
      this.databaseObj.executeSql(`
      SELECT * FROM ${this.table_name} WHERE pid = ${pid}
      `
        , [])
        .then((res) => {
          // this.trainData = {};
          if (res.rows.length > 0) {
            for (var i = 0; i < res.rows.length; i++) {
              // let newItem = res.rows.item(i)
              let newItem = res.rows.item(i);
              newItem.Points = JSON.parse(newItem.Points);
              this.trainData = newItem;
              console.log(this.trainData);
            }
            resolve(true);
          }
        })
        .catch(e => {

          resolve(false);
        });
    });
  }

  async presentToastWithOptions(message, color, rewardAAdd?, position?) {
    const toast = await this.toastController.create({
      message: message,
      position: position ? position : 'top',
      color: color,
      duration: 2000
    });
    toast.present().then(() => {
      if (rewardAAdd) {
        this.rewardAdd();
      }
    });
  }
  setLonLat(points) {
    // points = points.replace(/.$/,"`");
    let selectedPoints: any[] = points;
    // let selectedPoints: any[] = points;
    console.log(selectedPoints);
    // tslint:disable-next-line:variable-name
    this.latituede = selectedPoints[0].coordinates[1];
    this.longitude = selectedPoints[0].coordinates[0];
  }
  setLonLatCustomLoad(points) {
    // points = points.replace(/.$/,"`");
    let selectedPoints: any[] = points;
    // let selectedPoints: any[] = points;
    console.log(selectedPoints);
    // tslint:disable-next-line:variable-name
    this.latituede = selectedPoints[0].geometry.coordinates[1];
    this.longitude = selectedPoints[0].geometry.coordinates[0];
  }
  getCustomLoadMarkers(points: any) {
    // points = points.toString().replace('"','`');
    console.log('get');
    // points = points.replace(/.$/,"`");
    let selectedPoints: any[] = points;
    // let selectedPoints: any[] = points;
    console.log(selectedPoints);
    // tslint:disable-next-line:variable-name
    this.latituede = selectedPoints[0].geometry.coordinates[1];
    this.longitude = selectedPoints[0].geometry.coordinates[0];
    console.log("this.latituede");
    console.log(this.latituede);
    console.log(this.longitude);
    console.log("this.longitude");
    let tripCoordinates: any[] = [];
    for (let _i = 0; _i < selectedPoints.length; _i++) {
      if (_i >= 0) {
        this.addMarkersToMap(selectedPoints[_i].geometry.coordinates, _i + '', true);
        // if (_i == 0) {
        //   this.addMarkersToMap(selectedPoints[_i].coordinates, name, true);
        // } else {
        //   this.addMarkersToMap(selectedPoints[_i].coordinates, _i + '', false);
        // }
        let latLng = {
          'lat': selectedPoints[_i].geometry.coordinates[1],
          'lng': selectedPoints[_i].geometry.coordinates[0]
        };
        tripCoordinates.push(latLng);
      }
    }

    var tripPath = new google.maps.Polyline({
      path: tripCoordinates,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });
    tripPath.setMap(this.map);
  }
  getLonLat(selectedPoints) {
    let tripCoordinates: any[] = [];
    this.latituede = selectedPoints[0].coordinates[1];
    this.longitude = selectedPoints[0].coordinates[0];
    for (let _i = 0; _i < selectedPoints.length; _i++) {
      if (_i >= 0) {
        let latLng = {
          'lat': selectedPoints[_i].coordinates[1],
          'lng': selectedPoints[_i].coordinates[0]
        };
        tripCoordinates.push(latLng);
      }
    }
    return tripCoordinates;
  }
  getMarkers(points: any, name: any) {
    // points = points.toString().replace('"','`');
    console.log('get');
    // points = points.replace(/.$/,"`");
    let selectedPoints: any[] = points;
    // let selectedPoints: any[] = points;
    console.log(selectedPoints);
    // tslint:disable-next-line:variable-name
    this.latituede = selectedPoints[0].coordinates[1];
    this.longitude = selectedPoints[0].coordinates[0];
    console.log("this.latituede");
    console.log(this.latituede);
    console.log(this.longitude);
    console.log("this.longitude");
    let tripCoordinates: any[] = [];
    for (let _i = 0; _i < selectedPoints.length; _i++) {
      if (_i >= 0) {
        this.addMarkersToMap(selectedPoints[_i].coordinates, _i + '', true);
        // if (_i == 0) {
        //   this.addMarkersToMap(selectedPoints[_i].coordinates, name, true);
        // } else {
        //   this.addMarkersToMap(selectedPoints[_i].coordinates, _i + '', false);
        // }
        let latLng = {
          'lat': selectedPoints[_i].coordinates[1],
          'lng': selectedPoints[_i].coordinates[0]
        };
        tripCoordinates.push(latLng);
      }
    }

    var tripPath = new google.maps.Polyline({
      path: tripCoordinates,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });
    tripPath.setMap(this.map);
  }
  // Delete single row 
  deleteRow(item) {
    return new Promise((resolve, reject) => {
      this.databaseObj.executeSql(`
          DELETE FROM ${this.table_name} WHERE pid = ${item}
        `
        , [])
        .then((res) => {
          this.presentToastWithOptions("Successfully removed", "success");
          resolve(true);
        })
        .catch(e => {
          this.presentToastWithOptions("Record deletion error", "danger");
          resolve(false);
        });
    });
  }
  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }
  remove(id) {
    this.presentConfirm("Do you need to remove this list", "Check again..?", "Yes, Proceed please", id);
  }

  saveChanges() {
    this.presentConfirm('Are you sure?', 'You cannot revert this after your confirmation', 'Let me check again', 'Yes,proceed');
  }
  async presentConfirmRemove(message, subHeader, cancelText, okTex) {
    let alert = await this.alertCtrl.create({
      message: message,
      subHeader: subHeader,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.canMarkerRemove = false;
          }
        },
        {
          text: okTex,
          handler: () => {
            this.canMarkerRemove = true;
            this.presentToastWithOptions("You can click any marker and remove that", "", null, 'top');
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  async presentConfirm(message, subHeader, cancelText, okTex) {
    let alert = await this.alertCtrl.create({
      message: message,
      subHeader: subHeader,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: () => {
            this.updateRecord(this.trainData);
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  async presentConfirmAddNewMarker(message, subHeader, cancelText, okTex, isModify?) {
    let alert = await this.alertCtrl.create({
      message: message,
      subHeader: subHeader,
      inputs: [
        {
          name: 'marker_number',
          type: 'text',
          placeholder: 'Marker'
        }],
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: (data) => {
            if (!this.isInAppDataLoad) {
              if (parseInt(data.marker_number)>=0 && parseInt(data.marker_number) <= this.trainData.Points.length) {
                this.currentMarkerNumber = parseInt(data.marker_number);
                this.canMarkerActive = true;
                this.canMarkerRemove = false;
              } else {
                if(isModify && this.canMarkerActive){
                  this.canMarkerActive = true;
                }else{
                  this.canMarkerActive = false;
                }
                
                this.presentToastWithOptions("Please give a valid marker number", "danger");
              }

              // this.addNewMarker(cordinates, data.marker_number);
              // this.trainData.Points.splice(parseInt(data.marker_number), 0, {type: "Point", coordinates: [parseFloat(cordinates.lng().toFixed(6)), parseFloat(cordinates.lat())] });
              // this.getClusterMarkers(null);
            } else {
              // this.addNewCustomLoadMarker(cordinates, data.marker_number);
            }
          }
        }
      ]
    });
    alert.present();
  }
  addNewMarker(after, possission) {
    console.log(after.lat());
    console.log(after.lng());
    this.trainData.Points.splice(parseInt(possission), 0, { type: "Point", coordinates: [parseFloat(after.lng().toFixed(6)), parseFloat(after.lat())] });
    let tepTData = JSON.parse(JSON.stringify(this.trainData));
    this.trainData = null;
    this.trainData = tepTData;
    this.setLonLat(this.trainData.Points);
    // this.markerCluster.setMap(null);
    this.getClusterMarkers(null);
  }
  addNewCustomLoadMarker(after, possission) {
    console.log(after.lat());
    console.log(after.lng());
    this.tempData.features.splice(parseInt(possission), 0, { type: "Feature", properties: {}, geometry: { type: "Point", coordinates: [after.lng(), after.lat()] } });
    this.trainData['Points'] = this.convertGeoJsonToLocalType(this.tempData.features);

    let tepTData = JSON.parse(JSON.stringify(this.trainData));
    let tepTempData = JSON.parse(JSON.stringify(this.tempData));
    this.trainData = null;
    this.tempData = null;

    this.trainData = tepTData;
    this.tempData = tepTempData;

    this.setLonLat(this.trainData.Points);
    this.map = null;
    this.loadMap();
    this.getClusterMarkers(null);
  }
  dowloadFileConfirm() {
    this.presentConfirmDownload('Do you want to download this?', 'File will be download as GEO JSON', 'No', 'Proceed');
  }

  async presentConfirmDownload(message, subHeader, cancelText, okTex) {
    let alert = await this.alertCtrl.create({
      message: message,
      subHeader: subHeader,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: () => {
            this.dowloadFile(true);
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  removePath(id: any) {
    this.presentConfirm('Do you need to remove this path?', 'No,Thanks', 'Yes,Please Proceed', id);
  }
  setMapNull() {
    this.map = this.map.setMap(null);
  }
  addMarkersToMap(train, tite, status) {
    ///////////////////////////////////////////////

    ///////////////////////////////////////////////

    let museumMarker = new google.maps.Marker();
    console.log(train);
    if (status) {
      const position = new google.maps.LatLng(train[1], train[0]);
      museumMarker = new google.maps.Marker({
        animation: google.maps.Animation.DROP, draggable: true, position, title: tite, label: tite
      });

    } else {
      const position = new google.maps.LatLng(train[1], train[0]);
      museumMarker = new google.maps.Marker({ draggable: true, position, label: tite });

    }
    // this.map.addListener(museumMarker, 'dragend', function()
    // {
    //     console.log(museumMarker.getPosition().lat());
    // });
    google.maps.event.addListener(museumMarker, 'dragend', () => {
      console.log(museumMarker.getPosition().lat());
      if (this.isInAppDataLoad) {
        this.modifyMarker(museumMarker);
      } else {
        this.modifyCustomLoadMarker(museumMarker);
      }
    });
    google.maps.event.addListener(museumMarker, 'click', () => {
      console.log(museumMarker.getPosition().lat());
      this.presentConfirmTempRemoveMarker('Do you want to remove ' + museumMarker.label + ' marker?', 'Please make shure to save after your changes', 'No', 'Remove', museumMarker.label);
    });
    museumMarker.setMap(this.map);
    // google.maps.event.addListener(museumMarker, 'dragend', function()
    // {
    //   console.log(museumMarker.getPosition().lat());
    // });
  }
  async presentConfirmTempRemoveMarker(message, subHeader, cancelText, okTex, possision) {
    let alert = await this.alertCtrl.create({
      message: message,
      subHeader: subHeader,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: () => {
            if (!this.isInAppDataLoad) {
              this.tempRemoveMarker(possision);
            } else {
              this.tempRemoveCustomLoadMarker(possision);
            }
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  resetAll() {
    this.presentConfirmResetAll('Unsaved changes will be lost', 'Do you want to reset?', 'No,Wait..!', 'Yes,Please');
  }
  async presentConfirmResetAll(message, subHeader, cancelText, okTex) {
    let alert = await this.alertCtrl.create({
      message: message,
      subHeader: subHeader,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: () => {
            this.activeSaveButton = false;
            this.canMarkerActive = false;
            this.currentMarkerNumber = 0;
            this.canMarkerRemove = false;
            this.resetAllMarkers();
          }
        }
      ]
    });
    alert.present();
  }
  async resetAllMarkers() {
    await this.getRows(this.trainData.pid).then(() => {
      this.markerCluster.clearMarkers();
      this.getClusterMarkers(this.trainData.Points);
    });
  }
  modifyMarker(googleMapMarkerObject) {
    this.trainData.Points[parseInt(googleMapMarkerObject.label)].coordinates[0] = googleMapMarkerObject.getPosition().lng();
    this.trainData.Points[parseInt(googleMapMarkerObject.label)].coordinates[1] = googleMapMarkerObject.getPosition().lat();
    this.activeSaveButton = true;
  }
  modifyCustomLoadMarker(googleMapMarkerObject) {
    this.tempData.features[parseInt(googleMapMarkerObject.label)].geometry.coordinates[0] = googleMapMarkerObject.getPosition().lng();
    this.tempData.features[parseInt(googleMapMarkerObject.label)].geometry.coordinates[1] = googleMapMarkerObject.getPosition().lat();

    this.trainData.Points[parseInt(googleMapMarkerObject.label)].coordinates[0] = googleMapMarkerObject.getPosition().lng();
    this.trainData.Points[parseInt(googleMapMarkerObject.label)].coordinates[1] = googleMapMarkerObject.getPosition().lat();
  }
  tempRemoveMarker(possision) {
    this.markerCluster.clearMarkers();
    this.trainData.Points.splice(possision, 1);
    this.activeSaveButton = true;
    this.getClusterMarkers(this.trainData.Points);
  }
  tempRemoveCustomLoadMarker(possision) {
    this.tempData.features.splice(possision, 1);
    this.activeSaveButton = true;
    this.getClusterMarkers(this.tempData.features);
  }
  loadMap() {
    // this.geolocation.getCurrentPosition().then((resp) => {
    //   // resp.coords.latitude
    //   // resp.coords.longitude
    //   let mapOptions = {
    //     center: resp.coords,
    //     zoom: 15,
    //     mapTypeId: google.maps.MapTypeId.ROADMAP
    //   }
    //   alert(JSON.stringify( resp.coords));
    // this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    // }).catch((error) => {
    //   alert('Can not retrieve Location')
    //    console.log('Error getting location', error);
    //  });



    let mapOptions = {
      center: new google.maps.LatLng(this.latituede, this.longitude),
      zoom: 7,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    //### Add a button on Google Maps ...

    //### Add a button on Google Maps ...
    // var controlTrashUI = document.createElement('DIV');
    // controlTrashUI.style.cursor = 'pointer';
    // controlTrashUI.style.backgroundImage = "+";
    // controlTrashUI.style.backgroundColor = 'white'
    // controlTrashUI.style.height = '28px';
    // controlTrashUI.style.width = '25px';
    // controlTrashUI.style.content = '+'
    // controlTrashUI.style.top = '11px';
    // controlTrashUI.style.left = '150px';
    // controlTrashUI.title = 'Click to set the map to Home';
    // //myLocationControlDiv.appendChild(controlUI);

    // this.map.controls[google.maps.ControlPosition.LEFT_TOP].push(controlTrashUI);
    this.map.addListener('click', (mapsMouseEvent) => {
      // Close the current InfoWindow.

      // Create a new InfoWindow.
      if (this.canMarkerActive) {
        this.canMarkerRemove = false;
        let infoWindow = new google.maps.InfoWindow({ position: mapsMouseEvent.latLng });

        // this.presentConfirmAddNewMarker('Enter your new marker number', 'Do you want to add new marker here..?', 'No', 'Yes,Add this', mapsMouseEvent.latLng);
        // infoWindow.setContent(mapsMouseEvent.latLng.toString());
        // infoWindow.open(this.map);
        this.markerCluster.clearMarkers();
        this.trainData.Points.splice(this.currentMarkerNumber, 0, { type: "Point", coordinates: [parseFloat(mapsMouseEvent.latLng.lng().toFixed(6)), parseFloat(mapsMouseEvent.latLng.lat())] });
        this.canMarkerActive = false;
        this.activeSaveButton = true;
        this.getClusterMarkers(null);
      } else {
        this.presentToastWithOptions("Please give marker number using add button..!", "warning");
      }
    });

    // this.editPointsToMiddle();
  }
  onClickAddnewMarker() {
    this.presentConfirmAddNewMarker('Enter your new marker number', 'Do you want to add new marker?', 'No', 'Yes');
  }
  onClickChenageNewMarkerNumber() {
    this.presentConfirmAddNewMarker('Enter your new marker number', 'Do you want to update new marker number?', 'No', 'Yes',true);
  }
  onClickRemove() {
    this.presentConfirmRemove('', 'Do you want to remove markers?', 'No', 'Yes');
  }
  getAllTrains() {
    this._MainServicesService.get(`train/getAll`).subscribe(data => {
      console.log(data);
      this.trainData = data.data;
    });
  }
  hideBannerAdd() {
    const bannerConfig: AdMobFreeBannerConfig = {
      // add your config here
      // for the sake of this example we will just use the test config
      id: environment.admobBannerKey,
      isTesting: environment.isTestAddmob,
      autoShow: true
    };
    this.admobFree.banner.config(bannerConfig);
    this.admobFree.banner.prepare().then(() => {
      this.admobFree.banner.show();
    });
  }
  rewardAdd() {
    this.hideBannerAdd();
    const InterstitialVideoConfig: AdMobFreeInterstitialConfig = {
      // add your config here
      // for the sake of this example we will just use the test config
      id: environment.admobInterstitialKey,
      isTesting: environment.isTestAddmob,
      autoShow: true
    };
    if (this.mainServicesService.addClickCount === 0 || (environment.fullPageAddFreequanceInEvent <= this.mainServicesService.addClickCount)) {
      this.admobFree.interstitial.config(InterstitialVideoConfig);
      this.admobFree.interstitial.prepare().then(() => {
        this.admobFree.interstitial.show();
      });
      if (this.mainServicesService.addClickCount === 0) {
        this.mainServicesService.addClickCount = this.mainServicesService.addClickCount + 1;
      } else {
        this.mainServicesService.addClickCount = 0;
      }

    } else {
      this.mainServicesService.addClickCount = this.mainServicesService.addClickCount + 1;
    }
  }

}
export interface Museum {
  name: string;
  state: string;
  latitude: any;
  longitude: any
}