import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { AdMobFreeBannerConfig, AdMobFree } from '@ionic-native/admob-free/ngx';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.page.html',
  styleUrls: ['./search-filter.page.scss'],
})
export class SearchFilterPage implements OnInit, OnChanges {
  public radiusmiles = 1;
  public minmaxprice = {
    upper: 500,
    lower: 10
  };
  from: number;
  to: number
  current = 'Remove';
  list: any = {};
  errorMessage = '';
  allDistance = 0;
  constructor(public toastController: ToastController, private modalCtrl: ModalController,
    private admobFree: AdMobFree) { }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  ngOnInit() {
  }

  closeModal() {
    this.modalCtrl.dismiss({
      isAction: false
    });
  }
  validateInputs() {
    if (this.list.Points && this.list.Points.length) {
      if ((this.from < this.list.Points.length) && (this.to < this.list.Points.length)) {
        if (this.from > this.to) {
          this.errorMessage = '"From" values cannot be larger than "To" value';
          return false;
        } else {
          return true;
        }
      } else {
        if ((this.from > this.list.Points.length) && (this.to > this.list.Points.length)) {
          this.errorMessage = 'Input vales are in out of scope';
          return false;
        } else {
          if ((this.from > this.list.Points.length)) {
            this.errorMessage = '"From" values is in out of scope';
            return false;
          }
          if ((this.to > this.list.Points.length)) {
            this.errorMessage = '"To" values is in out of scope';
            return false;
          }
        }
      }
    }else{
      this.errorMessage = 'Tracking list not found';
            return false;
    }
  }
  getDistanceFromLatLonInM(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }
  calculateAllDistance() {
    let distanceTemp = this.calculateDistance(this.from, this.to);
    this.allDistance = parseFloat(distanceTemp ? distanceTemp.toFixed(6):'0');
    console.log(distanceTemp);
  }
  calculateDistance(from, to) {
    let tempDistance = 0;
    for (let index = 0; index < this.list.Points.length; index++) {
      if (this.list.Points[index] && this.list.Points[index + 1]) {
        const elemenFrom = this.list.Points[index];
        const elementTO = this.list.Points[index + 1];
        if (index >= from) {
          tempDistance += this.getDistanceFromLatLonInM(elemenFrom.coordinates[1], elemenFrom.coordinates[0],
            elementTO.coordinates[1], elementTO.coordinates[0]);
        }
        if (index +1 === to) {
          break;
        }
      }
    }
    return tempDistance;
  }
  actionModal() {
    console.log('validation');
    if (this.validateInputs()) {
      this.modalCtrl.dismiss({
        isRemove: this.current === 'Remove' ? true : false,
        from: this.from, to: this.to,
        result: this.current === 'Remove' ? this.createNumberArray(this.from, this.to) : null,
        isAction: true
      });
    } else {
      this.presentToastWithOptions(this.errorMessage, 'danger');
    }

  }
  radioChecked(value) {
    this.current = value;
    // this.presentToastWithOptions(value, "danger");
  }
  createNumberArray(from, to) {
    let array: any[] = [];
    for (let i = from; i <= to; i++) {
      array.push(i);
    }
    return array;
  }

  async presentToastWithOptions(message, color) {
    const toast = await this.toastController.create({
      message: message,
      position: 'top',
      color: color,
      duration: 2000
    });
    toast.present();
  }
}
