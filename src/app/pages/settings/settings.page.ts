import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, Platform, AlertController, ToastController, IonInfiniteScroll } from '@ionic/angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { NavigationExtras, Router } from '@angular/router';
import { File } from '@ionic-native/file/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { AdMobFreeBannerConfig, AdMobFree } from '@ionic-native/admob-free/ngx';
import { environment } from 'src/environments/environment.prod';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  lang: any;
  enableNotifications: any;
  paymentMethod: any;
  currency: any;
  enablePromo: any;
  enableHistory: any;
  trackedList: any = [];
  languages: any = ['English', 'Portuguese', 'French'];
  paymentMethods: any = ['Paypal', 'Credit Card'];
  currencies: any = ['USD', 'BRL', 'EUR'];
  databaseObj: SQLiteObject;
  searchWord = '';
  readonly database_name: string = "tracker.db";
  readonly table_name: string = "tracker_points";
  constructor(private platform: Platform,
    private sqlite: SQLite, public navCtrl: NavController,
    private router: Router, public alertCtrl: AlertController,
    public toastController: ToastController,
    private file: File,
    private emailComposer: EmailComposer,
    private admobFree: AdMobFree,
    private androidPermissions: AndroidPermissions) {
    this.platform.ready().then(() => {

      this.createDB();
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
        result => {
          if (result && result.hasPermission) {
            console.log('Has permission?', result.hasPermission);
            this.createDir();
          } else {
            this.androidPermissions.requestPermissions(
              [
                this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
                this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
              ]
            ).then(()=>{
              this.createDir();
            });
          }


        },
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
      );
    }).catch(error => {
      console.log(error);
    });
  }

  ngOnInit() {
  }
  createDir(){
    this.platform.ready().then(() =>{
      if(this.platform.is('android')) {
        this.file.checkDir(this.file.externalRootDirectory, environment.downloadDirectory).then(response => {
          console.log('Directory exists'+response);
        }).catch(err => {
          console.log('Directory doesn\'t exist'+JSON.stringify(err));
          this.file.createDir(this.file.externalRootDirectory, environment.downloadDirectory, false).then(response => {
            console.log('Directory create'+response);
          }).catch(err => {
            console.log('Directory no create'+JSON.stringify(err));
          }); 
        });
      }
    });
  }
  showBannerAdd() {
    const bannerConfig: AdMobFreeBannerConfig = {
      // add your config here
      // for the sake of this example we will just use the test config
      id: environment.admobBannerKey,
      isTesting: environment.isTestAddmob,
      autoShow: true
    };
    this.admobFree.banner.config(bannerConfig);
    this.admobFree.banner.prepare().then(() => {
      this.admobFree.banner.show();
    });
  }
  sendEmail(item) {
    let geoJsonTemp: any = this.creteGeoJson(item.Points);

    let objJsonB64 = this.dowloadFile(geoJsonTemp);
    let email = {
      to: '',
      attachments: [objJsonB64.split('~')[0] + objJsonB64.split('~')[1]],
      subject: 'GEO JSON',
      body: 'GEO Json',
      isHtml: true
    }

    this.emailComposer.open(email).then(result => {
      console.log("**************result");
      console.log(result);
      console.log("**************result");
      this.removeFile(objJsonB64.split('~')[1]);
    });
  }

  removeFile(fileName) {
    this.file.removeFile(this.file.externalRootDirectory + environment.downloadDirectory+'/', fileName)
      .then(_ => {
        // this.presentToastWithOptions('Directory is removed-> ' + this.file.applicationStorageDirectory, 'success');
      }
      ).catch(err => {
        // this.presentToastWithOptions('Directory not exist removed-> ' + this.file.applicationStorageDirectory, 'danger');
      }
      );
  }

  dowloadFile(geoJsonTemp: any[]) {
    let fileName = 'geojson_' + (new Date()).getTime() + '.json';
    this.file.writeFile(this.file.externalRootDirectory + environment.downloadDirectory+'/', fileName, JSON.stringify(geoJsonTemp), { replace: true })
      .then(_ => {
        this.presentToastWithOptions('Please check your <b>' + this.file.applicationStorageDirectory+ '</b> folder', 'success');
      }
      ).catch(err => {
        this.presentToastWithOptions('<b>'+this.file.applicationStorageDirectory+'</b> directory not exist', 'danger');
      }
      );
    return this.file.externalRootDirectory + environment.downloadDirectory+'/' + `~` + fileName;
  }
  dowloadFileConfirm(data: any[]) {
    if (data.length > 0) {
      this.presentConfirmDownload('Do you want to download this?', 'File will be download as GEO JSON', 'No', 'Proceed', data);
    } else {
      this.presentToastWithOptions('Please check again', 'danger');
    }
  }
  async presentConfirmDownload(message, subHeader, cancelText, okTex, data: any[]) {
    let alert = await this.alertCtrl.create({
      message: message,
      subHeader: subHeader,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: () => {
            this.dowloadFile(data);
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  creteGeoJson(data: any[]) {
    let geoJson = {
      "type": "FeatureCollection",
      "features": []
    };


    data.forEach((element, index) => {
      let temp = {
        "type": "Feature",
        "geometry": { "type": "Point", "coordinates": [element.coordinates[0], element.coordinates[1]] },
        "properties": {
          "name": index + ''
        }
      };
      geoJson.features.push(temp);
    });
    return geoJson;
  }
  async presentToastWithOptions(message, color) {
    const toast = await this.toastController.create({
      message: message,
      position: 'top',
      color: color,
      duration: 2000
    });
    toast.present();
  }
  openDetailsWithQueryParams(id) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        pid: JSON.stringify(id)
      }
    };
    this.router.navigate(['about'], navigationExtras);
  }
  // Retrieve rows from table
  createDB() {
    this.sqlite.create({
      name: this.database_name,
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.databaseObj = db;
        // this.presentToastWithOptions("DB created", "success");
        // this.createTable();
        this.showBannerAdd();
        this.getRows().then(res => {
        });
      })
      .catch(e => {
        // this.presentToastWithOptions("DB creation error in function", "danger");
      });
  }
  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      // if (data.length == 1000) {
      // event.target.disabled = true;
      // }
    }, 500);
  }
  getRows() {
    return new Promise((resolve, reject) => {
      this.databaseObj.executeSql(`
    SELECT * FROM ${this.table_name}
    `
        , [])
        .then((res) => {
          this.trackedList = [];
          if (res.rows.length > 0) {
            for (var i = 0; i < res.rows.length; i++) {
              let newItem = res.rows.item(i);
              newItem.Points = JSON.parse(newItem.Points);
              this.trackedList.push(res.rows.item(i));
            }
            console.log("this.trackedList");
            console.log(this.trackedList);
            resolve(true);
          }
        })
        .catch(e => {

          resolve(false);
        });
    });
  }
  editProfile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  logout() {
    this.navCtrl.navigateRoot('/');
  }
  async presentConfirm(message, subHeader, cancelText, okTex, id) {
    let alert = await this.alertCtrl.create({
      message: message,
      subHeader: subHeader,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: () => {
            this.deleteRow(id);
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }

  remove(id) {
    this.presentConfirm("You cannot revert this", "Do you need to remove this list", "Check again..?", "Yes, Proceed please", parseInt(id));
  }
  // Delete single row 
  deleteRow(item) {
    return new Promise((resolve, reject) => {
      this.databaseObj.executeSql(`
        DELETE FROM ${this.table_name} WHERE pid = ${item}
      `
        , [])
        .then((res) => {
          this.presentToastWithOptions("Record deleted", "success");
          resolve(true);
        })
        .catch(e => {
          this.presentToastWithOptions("Record deletion error", "danger");
          resolve(false);
        });
    });
  }
}
