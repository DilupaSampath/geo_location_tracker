import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  ModalController,
  Platform
} from '@ionic/angular';

// Modals
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';
import { ImagePage } from './../modal/image/image.page';
// Call notifications test by Popover and Custom Component.
// import { NotificationsComponent } from './../../components/notifications/notifications.component';
import { MainServicesService } from '../main-services.service';
import { Insomnia } from '@ionic-native/insomnia/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite/ngx';
import { Network } from '@ionic-native/network/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free/ngx';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-home-results',
  templateUrl: './home-results.page.html',
  styleUrls: ['./home-results.page.scss']
})
export class HomeResultsPage implements OnInit, OnChanges {
  ngOnInit(): void {
    if (this.isConnected()) {
      this.accuracy = true;
    }
    const bannerConfig: AdMobFreeBannerConfig = {
      // add your config here
      // for the sake of this example we will just use the test config
      id: environment.admobBannerKey,
      isTesting: environment.isTestAddmob,
      autoShow: true
    };
    this.admobFree.banner.config(bannerConfig);
    this.admobFree.banner.prepare().then(() => {
      this.admobFree.banner.show();
    });
    // console.log(this.isConnected());
    // this.checkLocationAccuracy();
  }

  currentTrain: any;
  watch: any;
  respValue: number = 0;
  status: boolean = true;
  searchKey = '';
  themeCover = 'assets/img/location_animation.gif';
  geoLocationArray = [];
  responseData: any;
  languageSelected: any;
  trainSelected: any;
  trainS: any;
  trainData: any[];
  trainLon: any;
  trainLat: any;
  isWatching: boolean;
  email: any;
  isPause = false;
  accuracy = false;
  databaseObj: SQLiteObject;
  public cashedLocationPoints: any[] = [];
  row_data: any = [];
  readonly database_name: string = "tracker.db";
  readonly table_name: string = "tracker_points";
  name_model: string = "";
  // Handle Update Row Operation
  updateActive: boolean;
  to_update_item: any;
  trackedLength = 0;
  idLastPromisseResolved = true;
  minDistance = 25;
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public _MainServicesService: MainServicesService,
    private geolocation: Geolocation,
    public toastController: ToastController,
    private insomnia: Insomnia,
    private emailComposer: EmailComposer,
    private backgroundMode: BackgroundMode,
    private platform: Platform,
    private sqlite: SQLite,
    private network: Network,
    private locationAccuracy: LocationAccuracy,
    private diagnostic: Diagnostic,
    private admobFree: AdMobFree
  ) {
    this.platform.ready().then(() => {
      // this.checkLocationAccuracy();
      this.getScreenWidth();
      this.getScrenHeight();
      this.diagnostic.isLocationEnabled().then(data => {

        if (data) {
          console.log(data);

        } else {
          this.presentAlertDataConnection('Please turn on your GPS', '', 'GPS need to be turn on to track cordinates', 'danger')
        }
      }).catch(err => {
        console.log(err);
      });
      this.createDB();
    }).catch(error => {
      // console.log("error ********DB");
      // this.presentToastWithOptions("DB creation error", "danger");
      console.log(error);
    })
    this.isWatching = false;
    this.email = {
      to: '',
      attachments: [],
      subject: 'Cordova Icons',
      body: JSON.stringify(this.cashedLocationPoints),
      isHtml: true
    }
    // this.getAllTrains();
  }

  errorCallbackGps = (e) => {
    console.error(e)
  };
  ngOnChanges(changes: SimpleChanges): void {
    this.respValue = this.cashedLocationPoints.length;
  }

  checkGpsOn() {
    this.diagnostic.isLocationEnabled().then(data => {

      if (data) {
        console.log(data);
      } else {
        this.presentAlertDataConnection('Please turn on your GPS', '', 'GPS need to be turn on to track cordinates', 'danger');
      }
    }).catch(err => {
      this.presentToastWithOptions("Ooops...! Somting went wrong in GPS..!!", "danger");
    });
  }
  isConnected(): boolean {
    let conntype = this.network.type;
    return conntype && conntype !== 'unknown' && conntype !== 'none';
  }
  getDistanceFromLatLonInM(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return ((d * 1000) > this.minDistance) ? true : false;
  }
  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }
  checkLocationAccuracy() {
    if (this.idLastPromisseResolved) {
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {

        if (canRequest) {
          // the accuracy option will be ignored by iOS
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            (data) => {
              console.log(data)
              this.idLastPromisseResolved = true;
              // this.ckeckGpsOnMessage = false;
            },
            error => console.log('Error requesting location permissions', error)
          );
        }

      });
    }
  }
  // Create DB if not there
  createDB() {
    this.sqlite.create({
      name: this.database_name,
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.databaseObj = db;
        // this.presentToastWithOptions("DB created", "success");
        this.createTable().then(res => {

        });
      })
      .catch(e => {
        // this.presentToastWithOptions("DB creation error in function", "danger");
      });
  }
  // Create table
  createTable() {
    return new Promise((resolve, reject) => {
      this.databaseObj.executeSql(`
    CREATE TABLE IF NOT EXISTS ${this.table_name}  (pid INTEGER PRIMARY KEY, Name varchar(255),Points TEXT)
    `, [])
        .then(() => {
          // this.presentToastWithOptions("Table created", "success");
          resolve(true);
        })
        .catch(e => {
          // this.presentToastWithOptions("Table creation error in function", "danger");
          resolve(false);
        });
    });
  }
  //Inset row in the table
  insertRow(pointListName: string, dataList) {
    // Value should not be empty
    return new Promise((resolve, reject) => {
      if (this.cashedLocationPoints.length < 0) {
        this.presentToastWithOptions("Cannot find tracked points", "danger");
        return;
      }

      this.databaseObj.executeSql(`
      INSERT INTO ${this.table_name} (Name,Points) VALUES ('${pointListName}','${JSON.stringify(dataList)}')
    `, [])
        .then(() => {
          this.presentToastWithOptions("Successfully saved the GEO Json", "success");
          resolve(true);
        })
        .catch(e => {
          this.presentToastWithOptions("Record saving error", "danger");
          resolve(false);
        });
    });
  }

  // Delete single row 
  deleteRow(item) {
    return new Promise((resolve, reject) => {
      this.databaseObj.executeSql(`
        DELETE FROM ${this.table_name} WHERE pid = ${item.pid}
      `
        , [])
        .then((res) => {
          this.presentToastWithOptions("Record deleted", "success");
          resolve(true);
        })
        .catch(e => {
          this.presentToastWithOptions("Record deletion error", "danger");
          resolve(false);
        });
    });
  }
  enableUpdate(item) {
    this.updateActive = true;
    this.to_update_item = item;
    this.name_model = item.Name;
  }
  storeGeoLocations(locationObject: any) {
    this.cashedLocationPoints.push(locationObject);
    this.respValue = this.cashedLocationPoints.length;
  }
  getJsonCount() {
    return this.cashedLocationPoints.length;
  }
  async presentAlertDataConnection(header, subheader, message, cssClass) {
    const alert = await this.alertCtrl.create({
      cssClass: cssClass,
      header: header,
      subHeader: subheader,
      message: message,
      buttons: [
        {
          text: 'Got it',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();
  }
  async presentToastWithOptions(message, color) {
    const toast = await this.toastController.create({
      message: message,
      position: 'top',
      color: color,
      duration: 2000
    });
    toast.present();
  }
  setLanguage(event) {
    let me = this;
    console.log(event.target.value);
    this.trainSelected = event.target.value;
    this._MainServicesService.setId(event.target.value);
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }
  //   [{
  //     "type": "Point",
  //     "coordinates": [
  //         80.02864122390747,
  //         7.131958239203191
  //     ]
  // }]
  trackingStatus(status: number) {
    if (true) {
      if (status == 0) {

        this.diagnostic.isLocationEnabled().then(data => {

          if (data) {
            console.log(data);
            if (this.accuracy) {
              if (this.isConnected()) {
                this.presentConfirm('Do you need to start tracking?', '<b style="font-size: smaller;color: #101ebf;"> Note :Enable high accuracy mode to better result</b>', 'No,Thanks', 'Yes,Please Proceed', 'start');
              } else {
                this.presentAlertDataConnection("Ooops..!", '', "Please turn on mobile data", "danger");
                setTimeout(() => {
                  this.accuracy = false;
                }, 1);
              }
            } else {
              this.presentConfirm('Do you need to start tracking ?', '<b style="font-size: smaller;color: #101ebf;"> Note :Enable high accuracy mode to better result</b>', 'No,Thanks', 'Yes,Please Proceed', 'start');
            }
          } else {
            this.presentAlertDataConnection('Please turn on your GPS', '', 'GPS need to be turn on to track cordinates', 'danger')
          }
        }).catch(err => {
          this.presentAlertDataConnection("Ooops..!", '', "Somthing went wrong in GPS connection", "danger");
        });


      } else if (status == 1) {
        this.diagnostic.isLocationEnabled().then(data => {

          if (data) {
            console.log(data);
            if (this.accuracy) {
              if (this.isConnected()) {
                this.isPause = false;
                this.themeCover = 'assets/img/map-icon-train-station.gif'
              } else {
                this.presentAlertDataConnection("Ooops..!", '', "Please turn on mobile data", "danger");
                setTimeout(() => {
                  this.accuracy = false;
                }, 1);
              }
            } else {
              this.isPause = false;
              this.themeCover = "assets/img/map-icon-train-station.gif";
            }

          } else {
            this.presentAlertDataConnection('Please turn on your GPS', '', 'GPS need to be turn on to track cordinates', 'danger')
          }
        }).catch(err => {
          this.presentAlertDataConnection("Ooops..!", '', "Somthing went wrong in GPS connection", "danger");
        });

      } else if (status == 2) {
        this.isPause = true;
        this.themeCover = "assets/img/map-icon-bar.gif";
      } else if (status == 3) {
        this.presentConfirm('Do you need to stop tracking ?', '<b style="font-size: smaller;color: #101ebf;"> Note :Enable high accuracy mode to better result</b>', 'No,Thanks', 'Yes,Please Proceed', 'stop');
      }

    } else {
      this.presentToastWithOptions("Please Select a Train First!!!", "danger");

    }
  }
  async presentConfirm(message, subHeader, cancelText, okTex, type) {
    this.backgroundMode.enable();
    let alert = await this.alertCtrl.create({
      message: this.accuracy ? '' : subHeader,
      subHeader: message,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: () => {
            if (type == 'start') {
              if (this.accuracy) {
                if (this.isConnected()) {
                  this.themeCover = "assets/img/map-icon-train-station.gif";
                  this.insomnia.keepAwake()
                    .then(
                      () => console.log('success'),
                      () => console.log('error')
                    );
                  this.respValue = this.cashedLocationPoints.length;
                  // checkLocationAccuracy
                  this.diagnostic.isLocationEnabled().then(data => {

                    if (data) {
                      console.log(data);
                      this.storeLocationData();
                      this.presentToastWithOptions("Tracking Started", "success");
                    }
                  }).catch(err => {

                  });
                } else {
                  setTimeout(() => {
                    this.accuracy = false;
                  }, 1);
                  this.presentAlertDataConnection("Ooops..!", '', "Please turn on mobile data", "danger");
                }
              } else {
                this.themeCover = "assets/img/map-icon-train-station.gif";
                this.insomnia.keepAwake()
                  .then(
                    () => console.log('success'),
                    () => console.log('error')
                  );
                this.respValue = this.cashedLocationPoints.length;
                this.storeLocationData();
                this.presentToastWithOptions("Tracking Started", "success");
              }

            } else {
              this.respValue = this.cashedLocationPoints.length;
              this.backgroundMode.enable();
              // this.themeCover = "https://cdn.dribbble.com/users/330174/screenshots/2695600/comp_2.gif";
              this.watch.unsubscribe();
              this.isWatching = false;
              // this._MainServicesService.postLocationData();
              this.presentToastWithOptions("Tracking Finished..!", "success");
              this.isPause = false;
              setTimeout(() => {
                this.themeCover = 'assets/img/location_animation.gif';
              }, 1000);
              let temp: any[] = JSON.parse(JSON.stringify(this.cashedLocationPoints));
              this.insomnia.allowSleepAgain()
                .then(
                  () => console.log('success'),
                  () => console.log('error')
                );
              this.cashedLocationPoints = [];
              // let al1 = this.alertCtrl.create({
              //   header: 'Tracked Points',
              //   message: JSON.stringify(this.cashedLocationPoints)
              // }).then(re => {
              //   re.present();
              // }).finally(() => {
              // });
              /*
             let email = {
               to: 'madsampath94@gmail.com',
               attachments: [],
               subject: 'Cordova Icons',
               body: JSON.stringify(this.cashedLocationPoints),
               isHtml: true
             }

            emailsend
             this.emailComposer.open(email).then(result => {
               console.log("**************result");
               console.log(result);
               console.log("**************result");
             });
             */
              this.presentPrompt(temp);
            }
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  setAaccuracy(event) {
    console.log(event.detail.checked);
    if (event.detail.checked) {
      if (this.isConnected()) {
        this.presentToastWithOptions('High accuracy mode selected', 'success');
        this.accuracy = event.detail.checked;
      } else {
        this.presentAlertDataConnection("Ooops..!", "Please turn on mobile data", "To enable highAccuracy mode you need data connecton to get salatile geo location", "danger");
        setTimeout(() => {
          this.accuracy = false;
        }, 1);
      }
    } else {
      this.presentToastWithOptions('Default mode selected', 'success');
      this.accuracy = event.detail.checked;
    }
  }
  async presentPrompt(dataList: any[]) {
    let alert = await this.alertCtrl.create({
      inputs: [
        {
          name: 'pointListName',
          placeholder: 'Tracked list Name'
        }
      ],
      buttons: [
        {
          text: "Don't save",
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
            this.respValue = this.cashedLocationPoints.length;
          }
        },
        {
          text: 'Save',
          handler: data => {
            if (data.pointListName) {
              // logged in!
              this.cashedLocationPoints = [];
              this.respValue = this.cashedLocationPoints.length;
              this.insertRow(data.pointListName, dataList).then(el => {

              });
            } else {
              // invalid login
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }
  getAllTrains() {
    this._MainServicesService.get(`train/getAll`).subscribe(data => {
      console.log(data);
      this.trainData = data.data;
    });
  }
  onclickSettings() {
    this.settings('The current minimum distance between two points is ' + this.minDistance+'m.<b style="font-size: smaller;color: #0f1740;">(minimum distance >=25 recomended)</b>', 'Minimum distance', 'Cancel', 'Change');
  }
  async settings(message, subHeader, cancelText, okTex) {
    // this.navCtrl.navigateForward('settings');
    // async presentConfirmAddNewMarker(message, subHeader, cancelText, okTex, cordinates?) {
    let alert = await this.alertCtrl.create({
      message: message,
      subHeader: subHeader,
      inputs: [
        {
          name: 'min_distance',
          type: 'number',
          placeholder: 'Minimum Distance'
        }],
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: (data) => {
            if (parseInt(data.min_distance) <= 5) {
              this.presentToastWithOptions("Your distance should be greater than 5", "danger");
            } else {
              this.minDistance = parseInt(data.min_distance);
            }

          }
        }
      ]
    });
    alert.present();
    // }
  }
  async onConfirmCancelTracking(message, subHeader, cancelText, okTex) {
    
      let alert = await this.alertCtrl.create({
        message: message,
        subHeader: subHeader,
        buttons: [
          {
            text: cancelText,
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: okTex,
            handler: () => {
              if(this.watch){
                this.watch.unsubscribe();
              }

              this.isWatching = false;
              // this._MainServicesService.postLocationData();
              this.presentToastWithOptions("Tracking stoped..!", "");
              this.isPause = false;
              this.themeCover = 'assets/img/location_animation.gif';
              this.insomnia.allowSleepAgain()
                .then(
                  () => console.log('success'),
                  () => console.log('error')
                );
              this.cashedLocationPoints = [];
            }
          }
        ]
      });
      alert.present();
    
  }
  cancelTracking(){
    this.onConfirmCancelTracking('When you click on confirm, all the current tracked points will be lost','Are you sure..?','Cancel','Yes,Please');
  }
  setId() {
    console.log(this.trainSelected);
    // this._MainServicesService.setId(id);
  }
  getTrackedPoints() {
    return this.cashedLocationPoints.length;
  }
  getScreenWidth(){
    let width = this.platform.width();
  }
  getScrenHeight(){
    let height = this.platform.height();
  }
  storeLocationData() {
    let options = {
      enableHighAccuracy: true
    };
    this.isWatching = true;
    let watch = this.geolocation.watchPosition({ enableHighAccuracy: this.accuracy });
    this.watch = watch
      .subscribe((resp) => {
        this.respValue = this.cashedLocationPoints.length;
        if (resp.coords.longitude != undefined) {
          this.respValue = this.cashedLocationPoints.length;
          this.isWatching = true;
          this.respValue = this.cashedLocationPoints.length;
          this.trainLon = resp.coords.longitude;
          this.trainLat = resp.coords.latitude;
          let lastLat = resp.coords.latitude;
          if (!this.isPause) {
            // this.storeGeoLocations(
            //   {
            //     "type": "Point",
            //     "coordinates": [
            //       resp.coords.longitude,
            //       resp.coords.latitude
            //     ]
            //   }
            // );
            if (this.cashedLocationPoints.length <= 0) {
              this.storeGeoLocations(
                {
                  "type": "Point",
                  "coordinates": [
                    resp.coords.longitude,
                    resp.coords.latitude
                  ]
                }
              );
            } else {
              let oldLat = this.cashedLocationPoints[this.cashedLocationPoints.length - 1].coordinates[1];
              let oldtLng = this.cashedLocationPoints[this.cashedLocationPoints.length - 1].coordinates[0];
              if (this.getDistanceFromLatLonInM(oldLat, oldtLng, resp.coords.latitude, resp.coords.longitude)) {
                this.storeGeoLocations(
                  {
                    "type": "Point",
                    "coordinates": [
                      resp.coords.longitude,
                      resp.coords.latitude
                    ]
                  }
                );
              }
            }


          }

        }

      });
    // for(let i=0;i<i+2;i++){
    //   if(!this.status){
    //     i=-100;
    //     this._MainServicesService.postLocationData();
    //   }else{
    //      let watch = this.geolocation.watchPosition();

    //     // this._MainServicesService.storeGeoLocations(currentLocation);
    //       }
    //   }

  }
  postLocationData() {
    // this._MainServicesService.postLocationData();

  }

  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: 'Tracking Settings',
      message: 'Current Tracking Time 2s',
      inputs: [
        {
          name: 'location',
          placeholder: 'Enter new tracking time',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Update',
          handler: async (data) => {
            console.log('Change clicked', data);
            this.currentTrain = data.location;
            const toast = await this.toastCtrl.create({
              message: 'Tracking time was change successfully',
              duration: 3000,
              position: 'top',
              closeButtonText: 'OK',
              showCloseButton: true
            });

            toast.present();
          }
        }
      ]
    });
    changeLocation.present();
  }

  async searchFilter() {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }

  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  // async notifications(ev: any) {
  //   const popover = await this.popoverCtrl.create({
  //     component: NotificationsComponent,
  //     event: ev,
  //     animated: true,
  //     showBackdrop: true
  //   });
  //   return await popover.present();
  // }

}
