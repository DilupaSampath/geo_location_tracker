export const environment = {
  production: false,
  socketUrl: 'http://159.65.248.18:3000',
  api_url: 'http://159.65.248.18:3000/',
  imageUploadPath: 'http://159.65.248.18:3200/uploads',
  apiKey: 'AIzaSyASr3NJ2EQUMz5tfrM7Fg_zgYY79m3yxE8',
  publicSiteUrl: 'http://liveTrains.com',
  isTestAddmob:false,
  downloadDirectory:'LiveGeoTracker',
  trackerDB: 'tracker.db',
  trackerPointsDB: 'tracker_points',
  admobBannerKey:'ca-app-pub-2562521245571264/4413290759',
  admobInterstitialKey:'ca-app-pub-2562521245571264/6845982598',
  admobRewardedKey:'ca-app-pub-2562521245571264/4551239167',
  fullPageAddFreequance:4,
  fullPageAddFreequanceInEvent:1
};
