import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;
    searchText = searchText.toLowerCase();
    return items.filter(it => {
      // 
      // console.log(k.toString().toLowerCase());})
      return it.Name.toString().toLowerCase().includes(searchText.toString().toLowerCase()) || it.pid.toString().toLowerCase().includes(searchText.toString().toLowerCase());
    });
  }

}